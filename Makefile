#silent by default
ifndef VERBOSE
.SILENT:
endif


start:
	docker compose up -d

stop:
	docker compose down
