#! /bin/bash
if [[ ! -e .env ]]; then
    cp .env.dist .env
fi

if [[ ! -e acme.json ]]; then
    touch acme.json
    chmod 600 acme.json
fi

if [[ ! -e traefik.yml ]]; then
    cp traefik.example.yml traefik.yml
fi
