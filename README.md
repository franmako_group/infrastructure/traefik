```bash
cp .env.dist .env

touch acme.json
chmod 600 acme.json

cp traefik.example.yml traefik.yml
```